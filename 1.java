package Test;
 
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(new File("file.txt"));
        String[] str = new String[21];

        while (scan.hasNext()) {
            String line = scan.nextLine();
            str = line.split(",");
        }

        int[] Arr = new int[str.length];

        System.out.println("Иначальный массив:");
        for (int i = 0; i <= str.length - 1; i++) {
            Arr[i] = Integer.parseInt(str[i]);
        }

        for (int i = 0; i <= str.length - 1; i++) {
            System.out.print(Arr[i] + " ");
        }
        System.out.println("\n\nСортировка по возрастанию:");

        Arrays.sort(Arr);
        for (int i = 0; i < Arr.length; i++) {
            System.out.print(Arr[i] + " ");
        }
        System.out.println("\n\nСортировка по убыванию:");

        for (int i = Arr.length - 1; i >= 0; i--) {
            System.out.print(Arr[i] + " ");
        }

    }
}