package Test;

public class Main {

    public static void main(String[] args) {
        int n = 20;
        long ch = 1;
        for (int i = 1; i <= n; i++) {
            ch = ch * i;
        }
        System.out.println("Факториал числа " + n + " равен: " + ch);
    }
}